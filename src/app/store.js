import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import personsReducer from "../features/persons/personsSlice";
import { customCounterReducer } from "./customCounterReducer";

import { setupListeners } from '@reduxjs/toolkit/query';
import { pokemonApi } from "../services/pokemon";
import {placeholderApi} from "../services/placeholder";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    persons: personsReducer,
    customCounter: customCounterReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
    [placeholderApi.reducerPath]: placeholderApi.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().
  concat(pokemonApi.middleware).
  concat(placeholderApi.middleware),
  devTools: process.env.NODE_ENV === "development"
});

//this must be called if we want refetchOnFocuse and refetchOnReconnect work correctly
setupListeners(store.dispatch)
