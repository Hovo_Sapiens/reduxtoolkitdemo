import { createAction, createReducer } from '@reduxjs/toolkit';

export const incrementByFive = createAction('customCounter/incrementByFive');
export const decrementByFour = createAction('customCounter/decrementByFour');
export const resetAll = createAction('customCounter/reset');

const initialState = {amount: 3};

export const customCounterReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(incrementByFive, state =>  {
            state.amount += 5
        })
        .addCase(decrementByFour, state => {
            state.amount -=4;
        })
        .addCase(resetAll, () => initialState)
        .addMatcher(
            action => action.type === 'counter/increment',
            state => {
                ++state.amount
            }
        )
        .addMatcher(
            action => action.type === 'counter/decrement',
            state => {
                --state.amount
            }
        )
});

export function selectAmount(state) {
    return state.customCounter.amount
}