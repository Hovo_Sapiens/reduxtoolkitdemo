import {createApi, fetchBaseQuery}  from "@reduxjs/toolkit/query/react";

export const placeholderApi = createApi({
    reducerPath: "placeholderApi",
    baseQuery: fetchBaseQuery({baseUrl: "https://jsonplaceholder.typicode.com/"}),
    keepUnusedDataFor: 10,
    endpoints: (build) => ({
        getAllPosts: build.query({
            query: () => "posts"
        }),
        getPostById: build.query({
            query: (id) => `posts/${id}`
        }),
        addPost: build.mutation({
            query: (body) => ({
                url: "posts",
                method: "POST",
                body
            }),
            keepUnusedDataFor: 20
        }),
        editPost: build.mutation({
            query: (data) => {
                const {id, ...body} = data;
                return {
                    url: `posts/${id}`,
                    method: "PUT",
                    body
                }
            },
            keepUnusedDataFor: 15
        }),
        deletePost: build.mutation({
            query: (id) => ({
                url: `posts/${id}`,
                method: "DELETE",
            }),
            keepUnusedDataFor: 5
        })
    })
})

export const { useGetAllPostsQuery, useGetPostByIdQuery, useAddPostMutation, useEditPostMutation, useDeletePostMutation } = placeholderApi;