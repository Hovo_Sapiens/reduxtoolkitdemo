import {basicApi} from "./index";

export const berryApi = basicApi.injectEndpoints({
    endpoints: (build) => ({
        getBerryById: build.query({
            query: (id) => `berry/${id}`
        })
    }),
    overrideExisting: false
})

export const { useGetBerryByIdQuery } = berryApi