// import {createApi, fetchBaseQuery}  from "@reduxjs/toolkit/query/react";
//
// export const pokemonApi = createApi({
//     reducerPath: "pokemonApi",
//     baseQuery: fetchBaseQuery({baseUrl: "https://pokeapi.co/api/v2/"}),
//     endpoints: (builder) => ({
//         getPokemonByName: builder.query({
//             query: (name) => `pokemon/${name}`
//         }),
//     })
// })

import {basicApi} from "./index";

export const pokemonApi = basicApi.injectEndpoints({
    endpoints: (build) => ({
        getPokemonByName: build.query({
            query: (name) => `pokemon/${name}`
        }),
        getPokemonWithZeroCache: build.query({
            query: (name) => `pokemon/${name}`,
            keepUnusedDataFor: 0
        })
    }),
    overrideExisting: false
})

export const { useGetPokemonByNameQuery, useGetPokemonWithZeroCacheQuery } = pokemonApi