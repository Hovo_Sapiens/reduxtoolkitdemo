import React from "react";
import styles from "./index.module.css";
import {useDispatch, useSelector} from "react-redux";
import {decrementByFour, incrementByFive, resetAll, selectAmount} from "../../app/customCounterReducer";

function CustomCounter() {
    const dispatch = useDispatch();
    const amount = useSelector(selectAmount);

    function addFiveHandler() {
        dispatch(incrementByFive());
    }
    function subtractFourHandler() {
        dispatch(decrementByFour());
    }
    function onResetHandler() {
        dispatch(resetAll());
    }

    return (
        <div className={styles.customCounterContainer}>
            <h3 className={styles.counterHeading}>Counter With Reducer Section</h3>
            <div className={styles.innerDiv}>
                <button className={styles.button} onClick={addFiveHandler}>+ 5</button>
                <p className={styles.amount}>{amount}</p>
                <button className={styles.button} onClick={subtractFourHandler}>- 4</button>
            </div>
            <button className={styles.resetBtn} onClick={onResetHandler}>Reset All</button>
        </div>
    )
}

export default CustomCounter;