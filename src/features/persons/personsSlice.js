import {createAsyncThunk, createSelector, createSlice, nanoid} from '@reduxjs/toolkit'
import {fetchFilters, fetchNewPerson} from "./personsApi";

const initialState = {
    filters: null,
    list: [
        {
            id: nanoid(),
            name: "Hovhannes",
            surname: "Poghosyan",
            age: 27
        },
        {
            id: nanoid(),
            name: "Zhirayr",
            surname: "Stepanyan",
            age: 28
        }
    ],
    status: `idle`
}

export const setFiltersAsync = createAsyncThunk(
    `persons/fetchFilters`,
    async (filters) => {
        const response = await fetchFilters(filters);
        return response.filters;
    }
);

export const addNewPersonAsync = createAsyncThunk(
    `persons/fetchNewPerson`,
    async (newPerson) => {
        const response = await fetchNewPerson(newPerson);
        return response.newPerson;
    }
)

const personsSlice = createSlice({
    name: "persons",
    initialState,
    reducers: {
        resetFilters: state => {
            state.filters = null;
        },
        deletePerson: (state, action) => {
            state.list = state.list.filter(item => item.id !== action.payload);
        },
        editPerson: (state, action) => {
            const {id, editableFields} = action.payload;
            const {list} = state;
            const person = list?.find(i => i.id === id);
            list[list.indexOf(person)] = {...person, ...editableFields};
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(setFiltersAsync.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(setFiltersAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                state.filters = action.payload;
            })
            .addCase(addNewPersonAsync.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(addNewPersonAsync.fulfilled, (state, action) => {
                state.status = 'idle';
                state.list?.unshift(action.payload);
            })
            .addMatcher(
                action => action.type.endsWith("reset"),
                () => initialState
            )
    }
})

export function selectFilters(state) {
    const {filters} = state.persons;
    return filters;
};
export function selectPersonsList(state) {
    const {list} = state.persons;
    return list;
};
export function selectPersonsStatus(state) {
    const {status} = state.persons;
    return status;
}
export function selectPersonById(state, id) {
    return state.persons.list?.find(person => person.id === id)
}

export const filteredPersons = createSelector([selectFilters, selectPersonsList], (filters, list) => {
    if (filters) {
        const {nameFilter, surnameFilter, ageFilter} = filters;
        return list.filter(item => (nameFilter ? item.name === nameFilter : item) &&
            (surnameFilter ? item.surname === surnameFilter : item) &&
            (ageFilter ? item.age === ageFilter : item))
    }
    return list;
})

export const {resetFilters, deletePerson, editPerson} = personsSlice.actions;

export default personsSlice.reducer;