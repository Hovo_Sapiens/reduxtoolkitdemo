import React, {useCallback, useMemo, useState} from "react";
import styles from "./index.module.css";
import {useDispatch, useSelector} from "react-redux";
import {deletePerson, filteredPersons, selectPersonsStatus} from "../personsSlice";
import EditPerson from "../editPerson";

function Table() {
    const dispatch = useDispatch();
    const personsList = useSelector(filteredPersons);
    const status = useSelector(selectPersonsStatus);

    const [editPersonId, setEditPersonId] = useState(null);

    const resetParentState = useCallback(() => setEditPersonId(null), []);

    const renderedPersons = useMemo(() => {
        return (
            personsList?.map(person => {
                const {name, surname, age, id} = person;

                return (
                    <tr key={id}>
                        <td className={styles.tableBodyCell}>{name}</td>
                        <td className={styles.tableBodyCell}>{surname}</td>
                        <td className={styles.tableBodyCell}>{age}</td>
                        <td className={styles.tableBodyCell}>
                            <button className={styles.editBtn} onClick={() => setEditPersonId(id)}>edit</button>
                            <button className={styles.deleteBtn} onClick={() => dispatch(deletePerson(id))}>-</button>
                        </td>
                    </tr>
                )
            })
        )
    }, [personsList]);

    return (
        <div style={{position: "relative", width: "fit-content"}}>
            <table className={styles[status === "idle" ? "table" : "tableWithLoader"]}>
                <thead>
                <tr>
                    <th className={styles.tableHeadingCell}>Name</th>
                    <th className={styles.tableHeadingCell}>Surname</th>
                    <th className={styles.tableHeadingCell}>Age</th>
                    <th className={styles.tableHeadingCell}>Action</th>
                </tr>
                </thead>
                <tbody>
                {renderedPersons}
                </tbody>
            </table>
            {editPersonId && <EditPerson personId={editPersonId} resetParentState={resetParentState} />}
        </div>
    )
}

export default Table;