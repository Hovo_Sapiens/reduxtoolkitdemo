export function fetchFilters(filters) {
    return new Promise((resolve) =>
        setTimeout(() => resolve({ filters }), 700)
    );
}

export function fetchNewPerson(newPerson) {
    return new Promise((resolve) =>
        setTimeout(() => resolve({newPerson}), 1000)
    );
}