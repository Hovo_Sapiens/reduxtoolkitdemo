import React, {useState} from "react";
import styles from "./index.module.css";
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import {editPerson, selectPersonById} from "../personsSlice";

function EditPerson({personId, resetParentState}) {
    const dispatch = useDispatch();
    const person = useSelector(state => selectPersonById(state, personId));

    const {name, surname, age} = person;

    const [inputName, setName] = useState(name);
    const [inputSurname, setSurname] = useState(surname);
    const [inputAge, setAge] = useState(age);

    const [emptyInputError, setEmptyInputError] = useState(false);

    const onEditHandler = () => {
        if (inputName && inputSurname && inputAge) {
            if (inputName === name && inputSurname === surname && inputAge === age) {
                resetParentState();
            }
            dispatch(editPerson({id: personId, editableFields: {name: inputName, surname: inputSurname, age: inputAge}}));
            resetParentState();
            return;
        }
        setEmptyInputError(true)
    };

    const onCancelHandler = () => {
        setName(name);
        setSurname(surname);
        setAge(age);
        setEmptyInputError(false);
    };

    return (
        <div className={styles.editContainer}>
            <div className={styles.editForm}>
                <div className={styles.filterColumn}>
                    <p className={styles.filterHeading}>Name</p>
                    <input type="text" placeholder="Input" value={inputName} onChange={e => setName(e.target.value)}/>
                </div>
                <div className={styles.filterColumn}>
                    <p className={styles.filterHeading}>Surname</p>
                    <input type="text" placeholder="Input" value={inputSurname} onChange={e => setSurname(e.target.value)}/>
                </div>
                <div className={styles.filterColumn}>
                    <p className={styles.filterHeading}>Age</p>
                    <input type="number" placeholder="Input" value={inputAge} onChange={e => setAge(Number(e.target.value))}/>
                </div>
                <div className={styles.btnContainer}>
                    <button className={styles.button} onClick={resetParentState}>{"< Back"}</button>
                    <button className={styles.button} onClick={onEditHandler}>Edit</button>
                    <button className={styles.button} onClick={onCancelHandler}>Cancel</button>
                </div>
                {emptyInputError && <p className={styles.emptyInputErrorMessage}>No empty inputs allowed!</p>}
            </div>
        </div>
    )
}

EditPerson.propTypes = {
    personId: PropTypes.string.isRequired,
    resetParentState: PropTypes.func.isRequired,
}

export default EditPerson;