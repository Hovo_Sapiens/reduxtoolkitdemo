import React, {useState} from "react";
import styles from "./index.module.css";
import {useDispatch, useSelector} from "react-redux";
import {nanoid} from "@reduxjs/toolkit";
import {addNewPersonAsync, selectPersonsStatus} from "../personsSlice";

function AddNewPerson() {
    const dispatch = useDispatch();
    const status = useSelector(selectPersonsStatus);

    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [age, setAge] = useState(0);

    const [emptyInputError, setEmptyInputError] = useState(false);

    const onCancelHandler = () => {
        setName("");
        setSurname("");
        setAge(0);
        setEmptyInputError(false);
    };
    const onAddHandler = () => {
        if (name && surname && age) {
            emptyInputError && setEmptyInputError(false)
            const newPerson = {
                id: nanoid(),
                name,
                surname,
                age
            }
            dispatch(addNewPersonAsync(newPerson));
            setName("");
            setSurname("");
            setAge(0);
            return;
        }
        setEmptyInputError(true)
    };

    return (
        <div className={styles.addNewPersonContainer}>
            {status === "loading" ? <div className={styles.disablingLayer} /> : null}
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Name</p>
                <input type="text" placeholder="Input" value={name} onChange={e => setName(e.target.value)}/>
            </div>
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Surname</p>
                <input type="text" placeholder="Input" value={surname} onChange={e => setSurname(e.target.value)}/>
            </div>
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Age</p>
                <input type="number" placeholder="Input" value={age} onChange={e => setAge(Number(e.target.value))}/>
            </div>
            <div className={styles.btnContainer}>
                <button className={styles.button} onClick={onAddHandler}>Add</button>
                <button className={styles.button} onClick={onCancelHandler}>Cancel</button>
            </div>
            {emptyInputError && <p className={styles.emptyInputErrorMessage}>You must fill all inputs!</p>}
        </div>
    )
}

export default AddNewPerson;