import React from "react";
import styles from "./Persons.module.css";
import Table from "./table";
import Filters from "./filters";
import AddNewPerson from "./addNewPerson";

function Persons() {
    return (
        <div className={styles.personsContainer}>
            <h3 className={styles.personsHeading}>Custom Persons Section</h3>
            <div className={styles.contentContainer}>
                <div>
                    <Filters />
                    <Table />
                </div>
                <AddNewPerson />
            </div>
        </div>
    )
}

export default Persons;