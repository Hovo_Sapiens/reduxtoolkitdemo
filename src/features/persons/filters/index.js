import React, {useState} from "react";
import styles from "./index.module.css";
import {useDispatch, useSelector} from "react-redux";
import {resetFilters, selectPersonsStatus, setFiltersAsync} from "../personsSlice";

function Filters() {
    const dispatch = useDispatch();
    const status = useSelector(selectPersonsStatus);

    const [nameFilter, setNameFilter] = useState("");
    const [surnameFilter, setSurnameFilter] = useState("");
    const [ageFilter, setAgeFilter] = useState(0);

    const onNameChangeHandler = e => {
        setNameFilter(e.target.value);
    };
    const onSurnameChangeHandler = e => {
        setSurnameFilter(e.target.value);
    };
    const onAgeChangeHandler = e => {
        setAgeFilter(Number(e.target.value));
    };

    const onResetHandler = () => {
        setNameFilter("");
        setSurnameFilter("");
        setAgeFilter(0);
        dispatch(resetFilters());
    }
    const onSearchHandler = () => {
        dispatch(setFiltersAsync({nameFilter, surnameFilter, ageFilter}));
    };

    return (
        <div className={styles.filters}>
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Name</p>
                <div className={styles[status === "loading" ? "disabledInput" : "enabledInput"]}>
                    <input type="text" placeholder="Input" value={nameFilter} onChange={onNameChangeHandler}/>
                </div>
            </div>
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Surname</p>
                <div className={styles[status === "loading" ? "disabledInput" : "enabledInput"]}>
                    <input type="text" placeholder="Input" value={surnameFilter} onChange={onSurnameChangeHandler}/>
                </div>
            </div>
            <div className={styles.filterColumn}>
                <p className={styles.filterHeading}>Age</p>
                <div className={styles[status === "loading" ? "disabledInput" : "enabledInput"]}>
                    <input type="number" placeholder="Input" value={ageFilter} onChange={onAgeChangeHandler}/>
                </div>
            </div>
            <button className={styles[status === "loading" ? "disabledButton" : "button"]} onClick={onResetHandler}>Reset</button>
            <button className={styles[status === "loading" ? "disabledButton" : "button"]} onClick={onSearchHandler}>Search</button>
        </div>
    )
}

export default Filters;