import React, {useState} from "react";
import PropTypes from "prop-types";

function IdFilter({value, callback}) {
    const [id, setId] = useState(value);

    const onChange = (e) => {
        setId(Number(e.target.value))
    }
    const onClick = () => {
        callback(id)
    }

    return (
        <div>
            <input type="number" placeholder="Type ID" value={id} onChange={onChange}/>
            <button onClick={onClick}>Find</button>
        </div>
    )
}

IdFilter.propTypes = {
    value: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired
}

export default IdFilter;