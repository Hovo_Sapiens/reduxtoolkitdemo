import React, {useCallback, useState} from "react";
import styles from "./Placeholder.module.css";
import {useGetPostByIdQuery} from "../../services/placeholder";
import IdFilter from "./IdFilter";

function SinglePost() {
    const [id, setId] = useState(1);

    const callback = useCallback((value) => {
        setId(value)
    }, []);

    const {data, isFetching, error} = useGetPostByIdQuery(id)

    return (
        <div className={styles.singlePost}>
            <IdFilter value={id} callback={callback} />
            {isFetching ? <h1>LOADING...</h1> : data ? <div className={styles.post}>
                <p><span>ID:</span> {data.id}</p>
                <p><span>User ID:</span> {data.userId}</p>
                <p><span>Body:</span> {data.body}</p>
                <p><span>Title:</span> {data.title}</p>
            </div> : error ? <h1>ERROR {error.status}</h1> : null}
        </div>
    )
}

export default SinglePost;