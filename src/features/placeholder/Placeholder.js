import React, { useMemo, useState} from "react";
import styles from "./Placeholder.module.css";
import AllPosts from "./AllPosts";
import SinglePost from "./SinglePost";
import {useAddPostMutation, useDeletePostMutation, useEditPostMutation} from "../../services/placeholder";
import {useDidUpdate} from "../../customHooks";

function Placeholder() {
    const [output, setOutput] = useState();

    const [addPost, addResult] = useAddPostMutation({
        fixedCacheKey: 'mutations-shared-result',
    });
    const [editPost, editResult] = useEditPostMutation();
    const [deletePost, deleteResult] = useDeletePostMutation();

    //**********
    //for demonstration of fixedCacheKey usage
    //When using fixedCacheKey, the originalArgs property is not able to be shared and will always be undefined.
    //**********
    // console.log({addResult, editResult, deleteResult})

    const onAddHandler = () => {
        addPost({
            title: 'added',
            body: 'once we were many, now we are few',
            userId: 1
        });
    }
    const onEditHandler = () => {
        editPost({
            id: 6,
            title: 'edited',
            body: 'in the jungle, the mighty jungle, the lion sleeps tonight',
            userId: 1,
        })
    }
    const onDeleteHandler = () => {
        deletePost(15)
    }
    const onResetHandler = () => {
        addResult.reset();
        editResult.reset();
        deleteResult.reset();
    }

    useDidUpdate(() => {
        const {data, isSuccess, isLoading} = addResult;
        isSuccess ? setOutput(data) : isLoading ? setOutput("LOADING...") : setOutput(undefined);
    }, [addResult]);
    useDidUpdate(() => {
        const {data, isSuccess, isLoading} = editResult;
        isSuccess ? setOutput(data) : isLoading ? setOutput("LOADING...") : setOutput(undefined);
    }, [editResult]);

    const outputPost = useMemo(() => {
        const isLoading = output === "LOADING...";
        const {id, userId, body, title} = output ?? {};

        return (
            <div className={styles.outputContainer}>
                {isLoading ?
                    <div className={styles.skeleton}>
                        <div><div /></div>
                        <div><div /></div>
                        <div><div /></div>
                        <div><div /></div>
                    </div>
                    : output ?
                    <div className={styles.post}>
                        <p><span>ID:</span> {id}</p>
                        <p><span>User ID:</span> {userId}</p>
                        <p><span>Body:</span> {body}</p>
                        <p><span>Title:</span> {title}</p>
                    </div>
                    : null}
            </div>
        )
    }, [output])

    return <div className={styles.placeholderContainer}>
        <h4 className={styles.placeholderHeading}>Placeholder With RTKQ Subsection</h4>
        <div className={styles.cont}>
            <AllPosts />
            <div style={{width: "47%", height: "450px"}}>
                <SinglePost />
                <div className={styles.btnContainer}>
                    <button onClick={onAddHandler}>add +</button>
                    <button onClick={onEditHandler}>edit</button>
                    <button onClick={onDeleteHandler}>delete -</button>
                    <button  className={styles.resetBtn} onClick={onResetHandler}>reset</button>
                </div>
                {outputPost}
            </div>
        </div>
    </div>
}

export default Placeholder;