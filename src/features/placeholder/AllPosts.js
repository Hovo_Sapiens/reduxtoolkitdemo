import React from "react";
import styles from "./Placeholder.module.css";
import {useGetAllPostsQuery} from "../../services/placeholder";

function AllPosts() {
    const {data, isFetching, isError, refetch} = useGetAllPostsQuery();

    return (
        <div className={styles.postsContainer}>
            <button onClick={refetch}>refetch</button>
            {data && (
                <div className={styles.posts}>
                    {data.map(post => {
                        const {id, userId, body, title} = post ?? {};
                        return <div className={styles.post} key={id}>
                            <p><span>ID:</span> {id}</p>
                            <p><span>User ID:</span> {userId}</p>
                            <p><span>Body:</span> {body}</p>
                            <p><span>Title:</span> {title}</p>
                        </div>
                    })}
                </div>
            )}
            <p className={styles.total}>TOTAL: {data?.length}</p>
        </div>
    )
}

export default AllPosts;