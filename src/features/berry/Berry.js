import React from "react";
import styles from "./Berry.module.css";
import {berryApi, useGetBerryByIdQuery} from "../../services/berry";
import {useDispatch, useSelector} from "react-redux";
import PropTypes from "prop-types";

function Berry({refetchWithInitiate, refetchOnMountOrArgChange}) {
    const dispatch = useDispatch();
    const id = useSelector(state => state?.counter?.value);

    const {
        data, // The latest returned result regardless of hook arg, if present.
        currentData, // The latest returned result for the current hook arg, if present.
        error, // The error result if present.
        isUninitialized, // When true, indicates that the query has not started yet.
        isLoading, // When true, indicates that the query is currently loading for the first time, and has no data yet. This will be true for the first request fired off, but not for subsequent requests.
        isFetching,// When true, indicates that the query is currently fetching, but might have data from an earlier request. This will be true for both the first request fired off, as well as subsequent requests.
        isSuccess, // When true, indicates that the query has data from a successful request.
        isError, // When true, indicates that the query is in an error state.
        refetch, // A function to force refetch the query
    } = useGetBerryByIdQuery(id, {refetchOnMountOrArgChange});

    const onReFetchHandler = () => {
        if (refetchWithInitiate) {
            dispatch(berryApi.endpoints.getBerryById.initiate(id, {subscribe: false, forceRefetch: true}));
            console.log("with initiate")
        } else {
            refetch();
            console.log("with refetch")
        }
    }

    return (
        <div className={styles.berryContainer}>
            <h4 className={styles.berryHeading}>Berries with RTKQ Subsection</h4>
            <p className={styles.intro}>This subsection is connected with default counter section, and requests are made according to its value</p>
            {isFetching ? <h1>LOADING...</h1> : isError ? <h1 style={{color: "red", textAlign: "center"}}>ERROR {error?.originalStatus}</h1> : data && (
                <div>
                    <p>ID: {data.id}</p>
                    <p>Name: {data.item.name}</p>
                    <p>Growth Time: {data.growth_time} years</p>
                    <p>Max. Size: {data.size} feet</p>
                    <p>Soil Dryness: {data.soil_dryness}%</p>
                </div>
            )}
            <button className={styles.refetch} onClick={onReFetchHandler}>re-fetch</button>

        </div>
    )
}

Berry.propTypes = {
    refetchWithInitiate: PropTypes.bool,
    refetchOnMountOrArgChange: PropTypes.bool
}
Berry.defaultProps = {
    refetchWithInitiate: false,
    refetchOnMountOrArgChange: true,
}

export default Berry;