import React, {useMemo, useState} from "react";
import styles from "./index.module.css";
import Pokemon from "../pokemon/Pokemon";
import Berry from "../berry/Berry";
import Placeholder from "../placeholder/Placeholder";

function RTKQSection() {
    const [hidePlaceholderSubsection, setHidePlaceholderSubsection] = useState(false);

    const hideBtn = useMemo(() => {
        return <button onClick={() => setHidePlaceholderSubsection(true)}>hide</button>
    }, [hidePlaceholderSubsection]);
    const showBtn = useMemo(() => {
        return <button onClick={() => setHidePlaceholderSubsection(false)}>show</button>
    }, [hidePlaceholderSubsection])

    const [berrySubsectionsCount, setBerrySubsectionsCount] = useState(2);

    const plusMinusBtns = useMemo(() => (
        <div>
            <button onClick={() => setBerrySubsectionsCount(prev => prev === 8 ? 8 : ++prev)}>+</button>
            <button onClick={() => setBerrySubsectionsCount(prev => prev === 0 ? 0 : --prev)}>-</button>
        </div>
    ), [])

    const berries = useMemo(() => {
        const returnValue = [];

        for (let i = 0; i < berrySubsectionsCount; ++i) {
            returnValue.push(<Berry key={i} refetchOnMountOrArgChange={i % 2 === 0} />)
        }

        return returnValue;
    }, [berrySubsectionsCount]);

    return (
        <div className={styles.RTKQContainer}>
            <h3 className={styles.RTKQHeading}>RTK Query Section</h3>
            {plusMinusBtns}
            <div className={styles.subsectionsContainer}>
                <Pokemon />
                <Berry refetchWithInitiate />
                {berries}
                {hidePlaceholderSubsection || <Placeholder/>}
            </div>
            {hidePlaceholderSubsection ? showBtn : hideBtn}
        </div>
    )
}

export default RTKQSection;