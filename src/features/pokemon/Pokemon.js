import React, {useState} from "react";
import styles from "./Pokemon.module.css";
import { useGetPokemonByNameQuery, useGetPokemonWithZeroCacheQuery } from "../../services/pokemon";

function Pokemon() {

    const [nameOne, setNameOne] = useState("bulbasaur");
    const [nameTwo, setNameTwo] = useState("ditto");

    const { data, error, isLoading } = useGetPokemonByNameQuery(nameOne, {
        //skips the request if true, default: false
        skip: false,
        //for automatically re-fetching after provided interval, default: 0 (off)
        pollingInterval: 0,
        //for altering the returned value of the hook to obtain a subset of the result, render-optimized for the
        // returned subset
        // selectFromResult: () => {},
        //for forcing the query to always refetch on mount (when true is provided). Allows forcing the query to refetch
        // if enough time (in seconds) has passed since the last query for the same cache (when a number is provided),
        // default: false
        refetchOnMountOrArgChange: false,
        //for refetch when the browser window regains focus, default: false
        refetchOnFocus: true,
        //for refetch when regaining a network connection, default: false
        refetchOnReconnect: true
    });
    const {data: zeroCacheData, error: zeroCacheError, isFetching: zeroCacheIsFetching} = useGetPokemonWithZeroCacheQuery(nameTwo);

    return (
        <div className={styles.pokemonContainer}>
            <h4 className={styles.pokemonHeading}>Pokemon With RTKQ Subsection</h4>
            <div>
                <div className={styles.singlePokemon}>
                    <p>Select pokemon's name</p>
                    <p className={styles.cacheTime}>With default caching time</p>
                    <select name="pokemon" id="pokemon" value={nameOne} onChange={e => setNameOne(e.target.value)}>
                        <option value="bulbasaur">Bulbasaur</option>
                        <option value="pikachu">Pikachu</option>
                        <option value="ditto">Ditto</option>
                        <option value="ivysaur">Ivysaur</option>
                        <option value="charmeleon">Charmeleon</option>
                        <option value="squirtle">Squirtle</option>
                    </select>
                    {error ? (
                        <>Oh no, there was an error</>
                    ) : isLoading ? (
                        <>Loading...</>
                    ) : data ? (
                        <>
                            <h3>{data.species.name}</h3>
                            <img src={data.sprites.front_shiny} alt={data.species.name} />
                        </>
                    ) : null}
                </div>
                <div className={styles.singlePokemon}>
                    <p>Select pokemon's name</p>
                    <p className={styles.cacheTime}>With 0 caching time</p>
                    <select name="pokemon" id="pokemon" value={nameTwo} onChange={e => setNameTwo(e.target.value)}>
                        <option value="bulbasaur">Bulbasaur</option>
                        <option value="pikachu">Pikachu</option>
                        <option value="ditto">Ditto</option>
                        <option value="ivysaur">Ivysaur</option>
                        <option value="charmeleon">Charmeleon</option>
                        <option value="squirtle">Squirtle</option>
                    </select>
                    {zeroCacheError ? (
                        <>Oh no, there was an error</>
                    ) : zeroCacheIsFetching ? (
                        <>Loading...</>
                    ) : zeroCacheData ? (
                        <>
                            <h3>{zeroCacheData.species.name}</h3>
                            <img src={zeroCacheData.sprites.front_shiny} alt={zeroCacheData.species.name} />
                        </>
                    ) : null}
                </div>
            </div>
        </div>
    )
}

export default Pokemon;