import React from 'react';
import { Counter } from './features/counter/Counter';
import Persons from "./features/persons/Persons";
import './App.css';
import CustomCounter from "./features/customCounter";
import RTKQSection from "./features/RTKQSection";

function App() {
  return (
    <div className="App">
      <h1 className="mainHeading">Redux Toolkit Demo</h1>
      <Counter />
      {/*<Persons />*/}
      {/*<CustomCounter />*/}
      <RTKQSection />
    </div>
  );
}

export default App;
